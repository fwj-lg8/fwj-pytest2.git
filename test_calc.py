import allure
import pytest
import yaml


# logging.basicConfig(level=logging.INFO)
# logger=logging.getLogger()

from test_code.calc import Calculator

# 打开yaml文件，并获取测试数据
with open('datas/calc.yml',encoding='utf-8')as f:
    data=yaml.safe_load(f)
    print(data)

# @pytest.fixture(scope='class')
# def get_clac():
#     print('开始测试')
#     calc=Calculator()
#     yield calc
#     print('测试结束')
#
# @pytest.fixture(scope='function')
# def teststart():
#     print('开始计算')
#     yield None
#     print('结束计算')




@allure.feature('计算机加法测试')
class Testcalc_sum:
    # def setup_class(self):
    #     print('开始测试')
    #     #实例化类
    #     self.calc=Calculator()
    #
    # def teardown_class(self):
    #     print('测试结束')

    # def setup(self):
    #     print('开始计算')
    #
    # def teardown(self):
    #     print('计算结束')

    @allure.story('加法成功用例')
    @allure.title('测试{a}+{b}={expect}')
    @pytest.mark.parametrize('a,b,expect',data[0])
    def test_success(self,get_clac,teststart,a,b,expect):
        result=get_clac.add(a,b)
        with allure.step(f'测试{a}+{b}={expect}'):
            assert result == expect

    @allure.story('加法失败用例')  # 测试方法标题
    @allure.title('测试{a}+{b}!={expect}')   #用例标题
    @pytest.mark.parametrize('a,b,expect',data[1]) #参数化
    def test_failure(self,get_clac,teststart,a,b,expect):
        result=get_clac.add(a,b)
        assert result!=expect

@allure.feature('计算器除法测试')
class Testcalc_div:
    # def setup_class(self):
    #     print('开始测试')
    #     #实例化类
    #     self.calc=Calculator()
    #
    # def teardown_class(self):
    #     print('测试结束')

    # def setup(self):
    #     print('开始计算')
    #
    # def teardown(self):
    #     print('计算结束')

    @allure.story('除法成功用例')
    @allure.title('测试{a}/{b}={expect}')
    @pytest.mark.parametrize('a,b,expect',data[2])
    def test_success(self,get_clac,teststart,a,b,expect):
        result=get_clac.div(a,b)
        assert result==expect

    @allure.story('除法失败用例')
    @allure.title('测试{a}/{b}！={expect}')
    @pytest.mark.parametrize('a,b,expect', data[3])
    def test_failture(self,get_clac, a, b, expect,teststart):
        try:
            result=get_clac.div(a,b)
            assert result!=expect
        # 若除数为0，则抛出异常提示信息
        except Exception:
            print('除数不能为0')

