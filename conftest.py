import pytest

from test_code.calc import Calculator
import logging

logging.basicConfig(level=logging.INFO)
logger=logging.getLogger()

@pytest.fixture(scope='session')
def get_clac():
    logger.info('开始测试')
    calc=Calculator()
    yield calc
    logger.info('测试结束')

@pytest.fixture(scope='function')
def teststart():
    logger.info('开始计算')
    yield None
    logger.info('结束计算')